var sampleApp = angular.module('sampleApp', []);
var index = 0; 

sampleApp.config(['$routeProvider',
  function($routeProvider ) {


    $routeProvider.
      when('/ChooseFloors', {
        templateUrl: 'templates/choose_floors.html',
        controller: 'ChooseFloorsController'
      }).
      when('/ChooseSections', {
        templateUrl: 'templates/choose_sections.html',
	controller: 'ChooseSectionsController'
      }).
      when('/ChooseWings', {
        templateUrl: 'templates/choose_wings.html',
        controller: 'ChooseWingsController'
      }).
      when('/ChooseSeats', {
        templateUrl: 'templates/choose_seats.html',
        controller: 'ChooseSeatsController'
      }).

     otherwise({
        redirectTo: '/ChooseFloors'
      });
}]);
 
function nextController($scope) {
	console.log(index);	
       $scope.next = function() {
	switch(index) {
		case 0: return '/ChooseSections';//	'templates/choose_sections';
			console.log(index);
			break;
		case 1: return '/ChooseWings';
			break;
		case 2: return '/ChooseSeats';
			break;
		default: return '/ChooseFloors';
		}
      }

       $scope.back = function() {
	switch(index) {
		case 1: return '/ChooseFloors';//	'templates/choose_sections';
			console.log(index);
			break;
		case 2: return '/ChooseSections';
			break;
		case 3: return '/ChooseWings';
			break;
		default: return '/ChooseSeats';
		}
      }
      $scope.backDisable = function() {
	if(index === 0) {
		return true;
	}
      }
      $scope.nextDisable = function() {
	if(index=== 3) {
		return true;
	}
      }

}
sampleApp.controller('ChooseFloorsController', function($scope) {
     
    $scope.message = 'This is where the floors are chosen';
    index = 0; 
    $scope.first = 'First';
	

});
 
 
sampleApp.controller('ChooseSectionsController', function($scope) {
 
    $scope.message = 'This is where the sections are chosen';
    index=1;
	

});

sampleApp.controller('ChooseWingsController', function($scope) {
 
    $scope.message = 'This is where the wings are chosen';
    index=2;
});
sampleApp.controller('ChooseSeatsController', function($scope) {
 
    $scope.message = 'This is where the seats are chosen';
    index=3;
});



